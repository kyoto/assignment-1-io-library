%define EXIT_CODE 60
%define NULLTERMINATED_SYMBOL 0
%define WRITE_CODE 1
%define STDOUT 1
%define NEWLINE_SYMBOL 0xA
%define BIT_DIVIDER 10
%define ACII_NUMBERS_CODE 0x30
%define MINUS_SYMBOL "-"
%define SUCCESS 1
%define ERROR 0
%define SPACE_SYMBOL 0x20
%define TAB_SYMBOL 0x9




section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_CODE
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rcx, rcx
    .cycle:
    cmp byte[rdi+rcx], NULLTERMINATED_SYMBOL
    je  .end
    inc rcx
    jmp .cycle
    .end:
    mov rax, rcx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, WRITE_CODE
    mov rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_SYMBOL	    
    
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdi, STDOUT
    mov rdx, 1
    mov rax, WRITE_CODE
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    js .neg
    jmp print_uint
    .neg:
    push rdi
    mov rdi, MINUS_SYMBOL
    call print_char
    pop rdi
    neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, BIT_DIVIDER
    push NULLTERMINATED_SYMBOL
    .splitting_by_digits:
    xor rdx, rdx
    div r8
    add rdx, ACII_NUMBERS_CODE
    push rdx
    test rax, rax
    jnz .splitting_by_digits
    .print:
    pop rdi
    test rdi, rdi
    jz .exit
    call print_char
    jmp .print
    .exit:
    ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    call string_length
    push rsi
    push rdi
    push rax
    mov rdi, rsi
    call string_length
    pop rcx
    pop rsi
    cmp rcx,rax
    jne .fail
    .cycle:
    cmp rcx, -1
    je .success
    mov r8b, byte[rdi+rcx]
    mov r9b, byte[rsi+rcx]
    cmp r8, r9
    jne .fail
    dec rcx
    jmp .cycle
    .fail:
    pop rsi
    mov rax, ERROR
    ret
    .success:
    pop rsi
    mov rax, SUCCESS
    ret
    
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push rax
    mov rsi, rsp
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi
    mov r9, rsi
    xor r10, r10
    .cycle_before_letters:
    call read_char
    cmp rax, NULLTERMINATED_SYMBOL
    je .end
    cmp rax, SPACE_SYMBOL
    je .cycle_before_letters
    cmp rax, TAB_SYMBOL
    je .cycle_before_letters
    cmp rax, NEWLINE_SYMBOL
    je .cycle_before_letters
    jmp .write
    .reading_cycle:
    call read_char
    cmp rax, NULLTERMINATED_SYMBOL
    je .end
    cmp rax, SPACE_SYMBOL
    je .end
    cmp rax, TAB_SYMBOL
    je .end
    cmp rax, NEWLINE_SYMBOL
    je .end
    jmp .write
    .write:
    mov byte[r8+r10], al
    inc r10
    dec r9
    test r9, r9
    jnz .reading_cycle
    jmp .check_last_letter
    .check_last_letter:
    test rax, rax
    jnz .error
    jmp .end
    .error:
    mov rax, ERROR
    ret
    .end:
    mov byte[r8+r10], NULLTERMINATED_SYMBOL
    mov rax, r8
    dec rcx
    mov rdx, r10
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
    xor rdx, rdx
    xor rax, rax
    mov r8, BIT_DIVIDER
    .reading_symbols:
    mov dl, byte[rdi+rcx]
    cmp rdx, ACII_NUMBERS_CODE
    jl .finish_reading
    cmp rdx, ACII_NUMBERS_CODE + 9
    jg .finish_reading
    sub rdx, ACII_NUMBERS_CODE
    jmp .shift_and_plus
    .shift_and_plus:
    push rdx
    mul r8
    pop rdx
    add rax, rdx
    inc rcx
    jmp .reading_symbols
    .finish_reading:
    mov rdx, rcx
    ret 


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], MINUS_SYMBOL
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    call string_length
    pop rdi
    cmp rax , rdx
    jg .fail
    .cycle:
    mov r8b, byte[rdi]
    mov byte[rsi], r8b
    inc rdi
    inc rsi
    test r8b, r8b
    jnz .cycle 
    ret
    .fail:
    mov rax, ERROR
    ret
